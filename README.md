# [Golang][Web] Add CSS in Go Web Applications.

A tiny app to demo how to add CSS and JavaScript in Go web applications.

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebAddingCSS.git
```

Run this program:

```
$ cd GolangWebAddingCSS
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Using CSS in Go web app](images/golang-web-adding-css.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
